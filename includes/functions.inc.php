<?php

    $MAPS_KEY = "AIzaSyBbIvLH-zj7dZmJyLUrX7y_dVEs1UpkAF8";

    ///////////////////////////////////////////////////////

    function placeQuery($query){

        global $MAPS_KEY;

        $result = file_get_contents(

            "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" .
            urlencode($query) . 
            "&key=" . 
            $MAPS_KEY

        );

        $obj = json_decode($result, true);

        return $obj;

    }

    function wikiQuery($query){

        $result = file_get_contents(

            "https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=" .
            urlencode($query) . 
            "&utf8=&format=json"

        );

        $obj = json_decode($result);

        return $obj;

    }

    function tagsQuery($tags, $query){

        global $MAPS_KEY;

        $LIMIT = 5;

        $global_results = array();

        foreach($tags as $tag){

            $result = file_get_contents(

                "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" .
                urlencode($query . " " . $tag) . 
                "&key=" . 
                $MAPS_KEY
    
            );
    
            $obj = json_decode($result, true);

            $x = 0;
            foreach($obj["results"] as $result){

                array_push($global_results, $result);

                if($x == $LIMIT) break;
            
            }

        }

        return $global_results;

    }


    ///////////////////////////////////////////////////////




?>