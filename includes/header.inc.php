<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>	
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width = device-width, initial-scale = 1">
		<title>Polo</title>
		<link rel="stylesheet" type="text/css" href="static/css/main.css?version=6">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
		<link rel="shortcut icon" type="image/png" href="https://i.pinimg.com/originals/20/27/1e/20271eec1a8a6c42f1b1679737e36c38.png"/>
        <script type="text/javascript" src="static/js/main.js?version=0"></script>
	</head>
	
	<body>