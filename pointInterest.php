<?php

    include("includes/functions.inc.php");

    $query = $_REQUEST["search"];

    $obj = wikiQuery($query);
    
    require_once "includes/header.inc.php";

?>

<div class="section center cards">
    <div class="container">
        <span class="title"> Here are some facts about <?php echo $query; ?> </span>

        <div class="row">
            <?php 
                foreach($obj->query->search as $result){
                    $url = 'https://www.google.com/search?sclient=psy-ab&site=&source=hp&btnG=Search&q=' . urlencode($result->title);
            ?>
                <a href='<?php echo $url; ?>' target="_blank">
                    <div class="col s12 m12">
                        <div class="card">
                            <div class="card-content">
                                <span class="card-title"><?php echo $result->title; ?> </span>
                                <p><?php echo $result->snippet; ?></p>
                                <br />
                                <p>Explore →</p>
                            </div>
                        </div>
                    </div>
                </a>
            <?php 
                }
            ?>
        </div>
    </div>
</div>


<?php
    require_once "includes/footer.inc.php";
?>