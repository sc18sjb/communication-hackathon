<?php
session_start();

require_once "includes/header.inc.php";
require_once "includes/functions.inc.php";
//require_once "includes/process.inc.php";


// PLZ PUT SOME CODE HERE PEOPLE IT IS LOOKING VERY SPARSE
?>

<div class="background">
    <div class="section center search">
        <div class="container">
            <form class="col s12" autocomplete="off" method="GET" action="" name="search-form">
                <div class="row center">
                    <a href="index.php">
                        <img src="static/img/logo_white.png" id="logo"/>
                    </a>

                    <br />

                    <div class="input-field col s12 m10">
                        <input placeholder="Type Something!" id="search" name="search" type="text">
                    </div>

                    <div class="input-field col s12 m2">
                        <button class="btn waves-effect waves-light" type="submit">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <?php
    if (isset($_GET['search']) && $_GET['search'] != "") {
        $text = urldecode($_GET['search']);
        ///// Location

        if (strpos($text, 'where') !== false && strpos($text, 'is')) {
            $text = str_replace('where', '', $text);
            $text = str_replace('is', '', $text);

            ?>
            <div class="section center cards">
                <div class="container">
                    <?php echo '<iframe scrolling="no" class="load-content" src="places.php?search=' . $text . '" style="width:100%;height:100%;" onload="resizeIframe(this)"></iframe>'; ?>
                </div>
            </div>
            <?php
        } else if (strpos($text, 'facts') !== false && strpos($text, 'about')) {
            $text = str_replace('facts', '', $text);
            $text = str_replace('about', '', $text);
            ?>
            <div class="section center cards">
                <div class="container">
                    <?php echo '<iframe scrolling="no" src="pointInterest.php?search=' . $text . '" style="width:100%;height:100%;" onload="resizeIframe(this)"></iframe>'; ?>
                </div>
            </div>
            <?php
        } else if (strpos($text, 'life') !== false && strpos($text, 'in')) {
            $text = str_replace('life', '', $text);
            $text = str_replace('in', '', $text);
            ?>
            <div class="section center cards">
                <div class="container">
                    <?php echo '<iframe scrolling="no" src="tags.php?search=' . $text . '" style="width:100%;height:100%;" onload="resizeIframe(this)"></iframe>'; ?>
                </div>
            </div>
            <?php
        }
    } else {
        ?>
        <div class="section center cards">
            <div class="container">
                <div class="row">
                    <a href="?search=where+is+places">
                        <div class="col s12 m6">
                            <div class="card">
                                <div class="card-image">
                                    <img src="static/img/boat.jpg">
                                    <span class="card-title">Places</span>
                                </div>    

                                <div class="card-content">
                                    <p>Explore →</p>
                                    <!-- <p>I am a very simple card. I am good at containing small bits of information.
                                    I am convenient because I require little markup to use effectively.</p> -->
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="?search=facts+about+stuff">
                        <div class="col s12 m6">
                            <div class="card">
                                <div class="card-image">
                                    <img src="static/img/test.png">
                                    <span class="card-title">Interests</span>
                                </div>    

                                <div class="card-content">
                                    <p>Explore →</p>
                                    <!-- <p>I am a very simple card. I am good at containing small bits of information.
                                    I am convenient because I require little markup to use effectively.</p> -->
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="?search=facts+about+sports">
                        <div class="col s12 m6">
                            <div class="card">
                                <div class="card-image">
                                    <img src="static/img/football.jpg">
                                    <span class="card-title">Sports</span>
                                </div>    

                                <div class="card-content">
                                    <p>Explore →</p>
                                    <!-- <p>I am a very simple card. I am good at containing small bits of information.
                                    I am convenient because I require little markup to use effectively.</p> -->
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="?search=where+is+food">
                        <div class="col s12 m6">
                            <div class="card">
                                <div class="card-image">
                                    <img src="static/img/food.png">
                                    <span class="card-title">Food</span>
                                </div>    

                                <div class="card-content">
                                    <p>Explore →</p>
                                    <!-- <p>I am a very simple card. I am good at containing small bits of information.
                                    I am convenient because I require little markup to use effectively.</p> -->
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>

    <?php } ?>
</div>

<?php
require_once "includes/footer.inc.php";

?>  