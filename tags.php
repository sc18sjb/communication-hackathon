<?php

    include("includes/functions.inc.php");

    $tags = array();

    if(isset($_POST["sports-check"]))
        array_push($tags, "sports club");

    if(isset($_POST["music-check"]))
        array_push($tags, "music club");

    if(isset($_POST["religion-check"]))
        array_push($tags, "religion club");

    if(isset($_POST["culture-check"]))
        array_push($tags, "culture club");

    if(isset($_POST["reading-check"]))
        array_push($tags, "reading club");

    if(isset($_POST["football-check"]))
        array_push($tags, "football club");

    if(isset($_POST["free-check"]))
        array_push($tags, "free events");

    if(isset($_POST["drinking-check"]))
        array_push($tags, "drinking events");

    if(isset($_POST["non-drinking-check"]))
        array_push($tags, "non drinking events");

    require_once "includes/header.inc.php";

?>

<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <style>
      /* Always set the map height explicitly to define the size of the div
 
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 30em;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body style="padding:1%;">

    <?php 

     if(!isset($_REQUEST['submit'])){

    ?>

    <div class="section cards">
        <div class="container">
            <div class="row">
                <div class="col s12 m12">
                    <span class="title">Select points of interest:</span>

                    <form action="" method="post">
                        <p>
                            <label>
                                <input name="sports-check" type="checkbox" />
                                <span>Sport</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input name="music-check" type="checkbox"  />
                                <span>Music</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input name="religion-check" type="checkbox" />
                                <span>Religion</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input name="culture-check" type="checkbox" />
                                <span>Culture</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input name="reading-check" type="checkbox" />
                                <span>Reading</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input name="football-check" type="checkbox"  />
                                <span>Football</span>
                            </label>
                        </p>

                        <p>
                            <label>
                                <input name="free-check" type="checkbox"  />
                                <span>Free events</span>
                            </label>
                        </p>

                        <p>
                            <label>
                                <input name="drinking-check" type="checkbox"  />
                                <span>Drinking events</span>
                            </label>
                        </p>

                        <p>
                            <label>
                                <input name="non-drinking-check" type="checkbox"  />
                                <span>Non drinking events</span>
                            </label>
                        </p>

                        <br>
                        <button class="btn waves-effect waves-light" type="submit" name="submit">Find clubs, communities, places
                            <i class="material-icons right"></i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php } ?>

    <?php

      if(isset($_REQUEST['submit'])){

        $query = $_REQUEST["search"];

        $obj = tagsQuery($tags, $query);
        $resultObj = json_encode($obj);

    ?>

    <div class="section center cards">
        <div class="container">
            <span class="title">Okay, have a look around!</span>

            <div id="map"></div>

            <div class="row">
                <?php 
                    foreach($obj as $result){
                        $url = "https://www.google.com/maps/search/?api=1&query=" . urlencode($result["name"]) . "&query_place_id=" . $result["id"];
                ?>

                    <a href='<?php echo $url; ?>' target="_blank">
                        <div class="col s12 m12">
                            <div class="card">
                                <div class="card-content">
                                    <span class="card-title"><?php echo $result['name']; ?> </span>
                                    <p>Explore →</p>
                                </div>
                            </div>
                        </div>
                    </a>
                <?php 
                    }
                ?>

            </div>
        </div>
    </div>


    <script>

      function initMap() {

        /////////////////

        let results = <?php echo $resultObj; ?>;

        let x;
        var locations = [];

        for(x = 0; x < results.length; x++){

            locations.push({lat: results[x]["geometry"]["location"]["lat"], lng: results[x]["geometry"]["location"]["lng"], name: results[x]["name"]});

        }

        /////////////////

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: new google.maps.LatLng(locations[0]["lat"], locations[0]["lng"]),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();
        
        var marker, i;

        for (i = 0; i < locations.length; i++) {  
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i]["lat"], locations[i]["lng"]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                infowindow.setContent(locations[i]["name"]);
                infowindow.open(map, marker);
                }
            })(marker, i));
        }

      }

    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=<?php echo $MAPS_KEY; ?>&callback=initMap">
    </script>
    
      <?php } ?>
  
<?php
    require_once "includes/footer.inc.php";
?>