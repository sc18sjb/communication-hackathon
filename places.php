<?php

    include("includes/functions.inc.php");

    $query = "";

    if (isset($_GET['search'])) {
      $query = $_GET['search'];
    }

    $obj = placeQuery($query);
    
    $resultObj = json_encode($obj);
    
    require_once "includes/header.inc.php";

?>    

<div class="section center cards">
  <div class="container">
    <span class="title"> Okay, I found some places for you </span>

    <div id="map"></div>
    
    <div class="row">
      <?php 
        foreach($obj["results"] as $result){
          $url = "https://www.google.com/maps/search/?api=1&query=" . urlencode($result["name"]) . "&query_place_id=" . $result["id"];
      ?>

      
        <a href='<?php echo $url; ?>' target="_blank">
          <div class="col s12 m12">
            <div class="card">
              <div class="card-content">
                <span class="card-title"><?php echo $result['name']; ?> </span>
                <p>Explore →</p>
              </div>
            </div>
          </div>
        </a>
      <?php 
        }
      ?>
    </div>
  </div>
</div>


    <script>

      function initMap() {

        /////////////////

        let results = <?php echo $resultObj; ?>["results"];

        let x;
        var locations = [];

        for(x = 0; x < results.length; x++){

            locations.push({lat: results[x]["geometry"]["location"]["lat"], lng: results[x]["geometry"]["location"]["lng"], name: results[x]["name"]});

        }

        /////////////////

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: new google.maps.LatLng(locations[0]["lat"], locations[0]["lng"]),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();
        
        var marker, i;

        for (i = 0; i < locations.length; i++) {  
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i]["lat"], locations[i]["lng"]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                infowindow.setContent(locations[i]["name"]);
                infowindow.open(map, marker);
                }
            })(marker, i));
        }

      }

    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=<?php echo $MAPS_KEY; ?>&callback=initMap">
    </script>

<?php
  require_once "includes/footer.inc.php";
?>